const initState = {
    authErr: null
}

const authReducer = (state = initState, action) => {

    switch (action.type) {
        case 'LOGIN_ERROR':
            return {
                ...state,
                authErr: 'Login Failed'
            }
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                authErr: null
            }
        case 'SIGNOUT_SUCCESS':
            console.log('Signout success');
            return state;
        case 'SIGNUP_SUCCESS':
            console.log('Signup success');
            return {
                ...state,
                authErr: null
            }
        case 'SIGNUP_ERROR':
            console.log('Signup error');
            return {
                ...state,
                authErr: action.err.message
            }
        default:
            return state;
    }
}

export default authReducer;