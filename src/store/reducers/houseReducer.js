const initState = {}

const houseReducer = (state = initState, action) => {
    switch (action.type) {
        case 'CREATE_HOUSE':
            console.log('Created HOUSE: ', action.house);
            return state;
        case 'CREATE_HOUSE_ERR':
            console.log('Created HOUSE Error: ', action.err);
            return state;
        default:
            return state;
    }
}

export default houseReducer;