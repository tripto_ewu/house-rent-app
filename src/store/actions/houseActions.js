export const createHouse = (house) => {
    return (dispatch, getState,{getFirebase, db}) => {
        //Make async call to database.
        //const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        console.log("created")
        db.collection('houses').add({
            ...house,
            ownerFirstName: profile.firstName,
            ownerLastName: profile.lastName,
            ownerId: authorId,
            createdAt: new Date()
        }).then((response) => {
            console.log("created")
            dispatch({type:'CREATE_HOUSE', response});
        }).catch((err) => {
            console.log(err)
            dispatch({type:'CREATE_HOUSE_ERR', err});
        })

    }
}

export const createRenter = (flat) => {
    return (dispatch, getState,{getFirebase, db}) => {
        //Make async call to database.
        //const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const ownerId = getState().firebase.auth.uid;
        console.log(profile)
        db.collection('renters').add({
            ...flat,
            ownerId: ownerId,
            active: true,
            createdAt: new Date()
        }).then((response) => {
            console.log("created")
            dispatch({type:'CREATE_HOUSE', response});
        }).catch((err) => {
            console.log(err)
            dispatch({type:'CREATE_HOUSE_ERR', err});
        })

    }
}