export const createProject = (project) => {
    return (dispatch, getState,{getFirebase, db}) => {
        //Make async call to database.
        //const firestore = getFirestore();
        const profile = getState().firebase.profile;
        const authorId = getState().firebase.auth.uid;
        
        db.collection('projects').add({
            ...project,
            authorFirstName: profile.firstName,
            authorLastName: profile.lastName,
            authorId: authorId,
            createdAt: new Date()
        }).then((response) => {
            dispatch({type:'CREATE_PROJECT', response});
        }).catch((err) => {
            dispatch({type:'CREATE_PROJECT_ERR', err});
        })

    }
}
export const updateProject = (project, doc) => {
    return (dispatch, getState,{getFirebase, db}) => {
        //Make async call to database.
        //const firestore = getFirestore();
        db.collection('projects').doc(doc).update({
            ...project,
            updatedAt: new Date()
        }).then((response) => {
            console.log("Successfully Updated", response)
        }).catch((err) => {
            console.log("Error")
        })

    }
}
export const deleteProject = (doc) => {
    return (dispatch, getState,{getFirebase, db}) => {
        db.collection('projects').doc(doc).delete().then(() => {
            console.log("deleted")
        }).catch((err) => {
            console.log('Error')
        })

    }
}
