import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Dashboard from './components/dashboard/Dashboard';
import HouseDetails from './components/houses/HouseDetails';
import SignIn from './components/auth/SignIn';
import SignUp from './components/auth/SignUp';
import CreateHouse from './components/houses/CreateHouse'
import UpdateProject from './components/projects/UpdateProject';
import CreateFlatRenter from './components/flats/CreateFlatRenter';
import './App.css'
class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <div>
          <Navbar />
          <Switch>
            <Route path="/" exact component={Dashboard} />
            <Route path="/house/:id" component={HouseDetails} />
            <Route path="/signin" component={SignIn} />
            <Route path="/signup" component={SignUp} />
            <Route path="/create" component={CreateHouse} />
            <Route path="/update/:id" component={UpdateProject} />
            <Route path="/create-flat-renter/:id" component={CreateFlatRenter} />
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}

export default App;
