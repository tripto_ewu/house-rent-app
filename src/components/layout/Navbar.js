import React from 'react';
import { Link } from 'react-router-dom';
import SignedInLinks from './SignedInLinks';
import SignedOutLinks from './SignedOutLinks';
import { connect } from 'react-redux';

import classes from './Navbar.module.css';

function Navbar(props) {
    const { auth, profile } = props;
    const links = auth.uid ? <SignedInLinks profile={profile} /> : <SignedOutLinks />;
    return (
           <nav className={"navbar navbar-expand-lg navbar-light bg-light mb-3 " + classes.navbarColor}>
               <Link to='/' className="navbar-brand " style={{fontFamily: 'Modak, cursive'}}>Easy Rent</Link>
               <div className="collapse navbar-collapse">
                    
                    {links}
               </div>
           </nav>
        
    )
}
const mapStateToProps = (state) => {
    //console.log(state.firebase.auth)
    return {
        auth: state.firebase.auth,
        profile: state.firebase.profile
    }
}
export default connect(mapStateToProps)(Navbar)