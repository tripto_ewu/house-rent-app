import React from 'react';
import {NavLink} from 'react-router-dom';

export default function SignedOutLinks() {
    return (
        <ul className="navbar-nav mr-auto">
            <li><NavLink className="nav-link" to="/signup">Signup</NavLink></li>
            <li><NavLink className="nav-link" to="/signin">Login</NavLink></li>
        </ul>
    )
}
