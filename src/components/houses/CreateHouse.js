import React, { Component } from 'react';
import { createHouse } from '../../store/actions/houseActions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import Styles from './CreateHouse.module.css'

class CreateHouse extends Component {

    state = {
        houseName: '',
        address: '',
        floorCount: null,
        flatCount: null,
        flatNames: [],
        houseId:null
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }

   
    handleSubmit = (e) => {
        e.preventDefault();
        let names = [];
        let char = 'A';
        for (let i = 1; i <= parseInt(this.state.floorCount); i++) {
            for (let j = 0; j < parseInt(this.state.flatCount); j++) {
                var name = i.toString() + " - " + String.fromCharCode(char.charCodeAt(0) + j)
                names.push(name);
            }
        }

        this.setState({
            flatNames: names,
            houseId:this.props.match.params.id
        }, () => {
            this.props.createHouse(this.state);
            this.props.history.push('/')
        })



    }
    render() {
        const { auth } = this.props;
        if (!auth.uid) {
            return <Redirect to='/signIn' />
        }
        return (
            <div className={"container " + Styles.regForm}>
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">Register New House</h5>
                    <div className="row">
                        <div className="input-field col s12 m6">
                            <label htmlFor="houseName">House Name</label>
                            <input type="text" id="houseName" onChange={this.handleChange} />
                        </div>
                        <div className="input-field col s12 m6">
                            <label htmlFor="address">Address</label>
                            <textarea className="materialize-textarea" id="address" onChange={this.handleChange} />
                        </div>
                        <div className="input-field col s12 m6">
                            <label htmlFor="floorCount">Number of floors</label>
                            <input type="text" id="floorCount" onChange={this.handleChange} />
                        </div>
                        <div className="input-field col s12 m6">
                            <label htmlFor="flatCount">Number of flats per floor</label>
                            <input type="text" id="flatCount" onChange={this.handleChange} />
                        </div>
                    </div>
                    <div className="input-field col s12 m6">
                        <button className="btn pink lignten-1 z-depth-0">Create</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createHouse: (house) => dispatch(createHouse(house))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateHouse);
