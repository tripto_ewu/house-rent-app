import React, { Component } from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import { deleteProject } from '../../store/actions/projectActions';
import { NavLink } from 'react-router-dom';
import moment from 'moment';
import Flat from '../flats/Renter';
import Classes from './HouseDetails.module.css';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import CreateFlatRenter from '../flats/CreateFlatRenter'

class HouseDetails extends Component {

    state = {
        flatNames: []
    }

    deleteHouseHandler = (doc) => {
        this.props.deleteHouse(doc);
        this.props.history.push('/')
    }


    searchFlat = (nameKey) => {
        console.log(this.props.renters)
        if (this.props.renters) {
            let renter = this.props.renters
            for (var i = 0; i < renter.length; i++) {
                if (renter[i].assignedFlatNo === nameKey) {
                    return renter[i];
                }
            }
        }
        else {
            return null;
        }

    }



    componentDidMount() {

        if (!this.props.auth.uid) return <Redirect to='/signIn' />
    }
    render() {
        const house = this.props.house
        console.log(house)
        if (house) {
            return (
                <div className="container">
                    <div className="card mb-3">
                        <div className={"card-header text-white " + Classes.grad}>
                            <h5 className="card-title">{house.houseName}</h5>
                            <h6 className="card-subtitle mb-2">{house.address}</h6>
                        </div>
                        <div className="card-body">
                            <div className="row">
                                <div className="col s6">
                                    <div>Owner {house.ownerFirstName} {house.ownerLastName}</div>
                                    <div>Created at {moment(house.createdAt.toDate()).calendar()}</div>
                                </div>
                                <div className="">
                                <button type="button" className="btn btn-outline-dark mx-3" data-toggle="modal" data-target="#createRenterModal">Create New Renter</button>
                                    <CreateFlatRenter id={this.props.match.params.id}/>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        {house.flatNames ? house.flatNames.map((flatName) => {
                            let flat = this.searchFlat(flatName)
                            return <div key={flatName} className="col-sm-12 col-md-6 col-lg-6"><Flat flatName={flatName} renter={flat} /></div>
                        }) : <div>No Data</div>
                        }
                    </div>



                </div>)

        }
        else {
            return (

                <div className="progress">
                    <div className="indeterminate"></div>
                </div>

            )
        }
    }
}


const mapStateToProps = (state, ownProps) => {
    console.log(state.firestore)
    const id = ownProps.match.params.id;
    const houses = state.firestore.data.houses;
    const renters = state.firestore.ordered.renters;
    const house = houses ? houses[id] : null;
    return {
        house: house,
        auth: state.firebase.auth,
        renters: renters
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProject: (doc) => dispatch(deleteProject(doc))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect((props) => {
        console.log(props.auth.uid)
        if (props.auth.uid) {
            return [
                { collection: 'houses', orderBy: ['createdAt', 'desc'], where: [['ownerId', '==', props.auth.uid]] },
                { collection: 'renters', orderBy: ['createdAt', 'desc'], where: [['ownerId', '==', props.auth.uid]] },
            ]
        }
        return [];
    })
)(HouseDetails);
