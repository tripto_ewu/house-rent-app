import React from 'react';
import HouseSummary from './HouseSummary';
import Classes from './Houses.module.css';
import { Link } from 'react-router-dom';
import moment from 'moment';



export default function Houses({ houses, deleteHouse }) {

    const getMonth = () => {
        var threeMonthsAgo = moment().subtract(3, 'months');
        console.log(threeMonthsAgo.format())
    }
    getMonth()
    return (

        <div className="project-list section">
            {houses && houses.map((house, index) => {
                return (
                    <div className="card" key={index}>
                        <Link to={'/house/' + house.id} style={{textDecoration:'none'}}>
                            <HouseSummary house={house} />
                        </Link>
                    </div>
                )
            })}
        </div >
    )
}

