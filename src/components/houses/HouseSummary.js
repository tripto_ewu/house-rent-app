import React from 'react';
import moment from 'moment';


export default function HouseSummary({ house }) {


    return (
        <div className={"card-body "}>
            <span className="card-title"><strong>{house.houseName}</strong></span>
            <p>{house.ownerFirstName} {house.ownerLastName}</p>
            <p className="grey-text">{moment(house.createdAt.toDate()).calendar()}</p>
        </div>
    )
}
