import React from 'react'
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import { deleteProject } from '../../store/actions/projectActions';
import { NavLink } from 'react-router-dom';
import moment from 'moment';
function ProjectDetails(props) {

    const deleteProjectHandler = (doc) => {
        props.deleteProject(doc);
        console.log("delete")
        props.history.push('/')
    }

    const { project, auth } = props;
    if (!auth.uid) return <Redirect to='/signIn' />
    console.log(project)

    if (project) {
        return (
            <div className="container section project-details">
                <div className="card z-depth-0">
                    <div className="card-content">
                        <span className="card-title">{project.title}</span>
                        <p>{project.content}</p>
                    </div>
                    <div className="card-action grey lighten-4 grey-text">
                        <div className="row">
                            <div className="col s6">
                                <div>Posted by {project.authorFirstName} {project.authorLastName}</div>
                                <div>Created at {moment(project.createdAt.toDate()).calendar()}</div>
                            </div>
                            <div className="col s6 right-align">
                                <NavLink className="blue-text" to={'/update/' + props.match.params.id}>Update</NavLink>
                                <a className="red-text" style={{ cursor: 'pointer' }} onClick={() => deleteProjectHandler(props.match.params.id)}>Delete</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>)

    }
    else {
        return (
            <div className="container center">
                <p>Loading project...</p>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const projects = state.firestore.data.projects;
    const project = projects ? projects[id] : null;
    return {
        project: project,
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        deleteProject: (doc) => dispatch(deleteProject(doc))
    }
}



export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetails);
