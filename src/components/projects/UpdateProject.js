import React, { Component } from 'react';
import {updateProject} from '../../store/actions/projectActions';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'


class UpdateProject extends Component {

    state = {
        title: this.props.project ? this.props.project.title : '',
        content: this.props.project ? this.props.project.content : ''
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id] : e.target.value
        })
    }

    handleSubmit= (e) =>{
        e.preventDefault();
        this.props.updateProject(this.state, this.props.match.params.id);
        this.props.history.push('/project/'+this.props.match.params.id)
    }
    componentDidMount(){
      
    }
   

    static getDerivedStateFromProps(props, state){
        
        if(props.project !== null && state.title === ''){
           
            
            return {
                title: props.project.title,
                content:props.project.content
            }
        }
        return state;
    }
    render() {
        const { auth } = this.props;
        if(!auth.uid){
            return <Redirect to = '/signIn' />
        }
        return (
            <div className="container">
                <form className="white" onSubmit={this.handleSubmit}>
                    <h5 className="grey-text text-darken-3">Update Task</h5>
                    <div className="input-field">
                        <label htmlFor="title">Title</label>
                        <input type="text"  defaultValue={this.state.title} id="title" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <label htmlFor="content">Content</label>
                        <textarea className="materialize-textarea" defaultValue={this.state.content} id="content" onChange={this.handleChange}/>
                    </div>
                    <div className="input-field">
                        <button className="btn pink lignten-1 z-depth-0">Update</button>
                    </div>
                </form>
            </div>
        )
    }
}

const mapStateToProps = (state, ownProps) => {
    const id = ownProps.match.params.id;
    const projects = state.firestore.data.projects;
    const project = projects ? projects[id] : null;
    return {
        project: project,
        auth: state.firebase.auth
    }
}

const mapDispatchToProps = (dispatch) => {
    return{
        updateProject: (project, doc) => dispatch(updateProject(project,doc))
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(UpdateProject)
