import React from 'react';
import ProjectSummary from './ProjectSummary';

import { Link } from 'react-router-dom'



export default function ProjectList({ projects, deleteProject }) {

    
    return (
        <div className="project-list section">
            {projects && projects.map((project, index) => {
                return (
                    <div className="card z-depth-0 project-summary" key={index}>
                        <Link to={'/project/' + project.id} >
                            <ProjectSummary project={project} />
                        </Link>
                    </div>
                )
            })}
        </div >
    )
}

