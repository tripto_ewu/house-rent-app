import React, { Component } from 'react';
import Notifications from './Notifications';
import Houses from '../houses/Houses';
import { connect } from 'react-redux';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import { Redirect } from 'react-router-dom';


class Dashboard extends Component {


    componentDidMount(){
        
    }

    render() {
        //console.log(this.props)
        const { houses, auth, notifications } = this.props

        if (!auth.uid) {
            return <Redirect to='/signIn' />
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col s12 m6">
                        <Houses houses={houses} />
                    </div>
                    <div className="col s12 m6">
                        <Notifications notifications={notifications} />
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {

        auth: state.firebase.auth,
        notifications: state.firestore.ordered.notifications,
        houses: state.firestore.ordered.houses,
    }
}

export default compose(
    connect(mapStateToProps),
    firestoreConnect((props) => {
        
        if (props.auth.uid) {
            
            return [
                { collection: 'houses', orderBy: ['createdAt', 'desc'], where: [['ownerId', '==', props.auth.uid]] },
                { collection: 'notifications', limit: 3, orderBy: ['time', 'desc'] },
            ]
        }
        return [];
    })
)(Dashboard);