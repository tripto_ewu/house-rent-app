import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom'
import { createRenter } from '../../store/actions/houseActions';
import { firestoreConnect } from 'react-redux-firebase';
import { compose } from 'redux';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import Classes from './CreateFlatRenter.module.css'


class CreateHouse extends Component {

    state = {
        firstName: '',
        lastName: '',
        address: '',
        assignedFlatNo: '',
        phoneNo: '',
        advanceAmount: '',
        rentPerMonth: '',
        rentDate: null
    }

    handleChange = (e) => {
        console.log(e)
        this.setState({
            [e.target.id]: e.target.value
        })
    }

    handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state)
        //this.props.createRenter(this.state);
        //this.props.history.push('/')
    }

    handleDateChange = date => {
        this.setState({
            rentDate: date
        });
    };
    componentDidUpdate() {


    }
    render() {

        const { auth } = this.props;
        if (!auth.uid) {
            return <Redirect to='/signIn' />
        }

        if (this.props.houses) {


            return (
                <div className="modal fade bd-example-modal-lg"
                    id="createRenterModal"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="exampleModal"
                    aria-hidden="true">
                    <div className="modal-dialog modal-dialog-centered modal-lg">
                        <div className="modal-content">
                            <div className={"modal-header text-white " + Classes.modalHeader}>
                                <h5 className="modal-title" id="exampleModalLongTitle">Create New Renter</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form style={{ margin: 0, boxShadow: "none" }} onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-sm-12 col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="firstName">First Name</label>
                                                <input type="text"
                                                    className="form-control" id="firstName"
                                                    placeholder="Enter email"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="lastName">Last Name</label>
                                                <input type="text"
                                                    className="form-control" id="lastName"
                                                    placeholder="Enter Last Name"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-12">
                                            <div className="form-group">
                                                <label htmlFor="address">Address</label>
                                                <textarea className="form-control"
                                                    id="address"
                                                    rows="2"
                                                    onChange={this.handleChange}>
                                                </textarea>
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="phoneNo">Phone Number</label>
                                                <input type="text"
                                                    className="form-control" id="phoneNo"
                                                    placeholder="Enter phone number"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="assignedFlatNo">Select Flat Number</label>
                                                <select className="form-control"
                                                    id="assignedFlatNo"
                                                    onChange={this.handleChange}>
                                                    {this.props.houses[this.props.id].flatNames.map((name) => {
                                                        return <option key={name} value={name}>Flat: {name}</option>
                                                    })
                                                    }
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="advanceAmount">Advance Amount</label>
                                                <input type="text"
                                                    className="form-control" id="advanceAmount"
                                                    placeholder="Enter advance amount"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div className="form-group">
                                                <label htmlFor="rentPerMonth">Rent Per Month</label>
                                                <input type="text"
                                                    className="form-control" id="rentPerMonth"
                                                    placeholder="Enter rent per month"
                                                    onChange={this.handleChange}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-sm-12 col-md-6">
                                            <div className={Classes.dateInput}>
                                                <label htmlFor="rentDate">Rent Date</label>
                                                <DatePicker id="rentDate"
                                                    dateFormat="dd/MM/yyyy"
                                                    className="form-control"
                                                    placeholderText='Enter rent date'
                                                    selected={this.state.rentDate}
                                                    onChange={this.handleDateChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="modal-footer mt-5">
                                        <button type="submit" className="btn btn-primary text-right">Submit</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            )
        }
        else {
            return <div>Loading</div>
        }
    }
}

const mapStateToProps = (state) => {
    return {
        auth: state.firebase.auth,
        houses: state.firestore.data.houses
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        createRenter: (flat) => dispatch(createRenter(flat))
    }
}

export default compose(
    connect(mapStateToProps, mapDispatchToProps),
    firestoreConnect((props) => {
        if (props.auth.uid) {
            return [
                { collection: 'houses', orderBy: ['createdAt', 'desc'], where: [['ownerId', '==', props.auth.uid]] },
            ]
        }
        return [];
    })
)(CreateHouse);

