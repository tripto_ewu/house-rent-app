import React from 'react'
import classes from './Renter.module.css';

export default function flat(props) {
    if (props.renter && props.renter.active) {
        let renterDetail = props.renter
        return (
            <div className="card mb-2">
                <div className={"card-header text-white " + classes.renterHeaderActive} style={{ cursor: 'pointer' }}>
                    <h4 className="card-title">{renterDetail.firstName} {renterDetail.lastName}</h4>
                    <h6 className="card-subtitle">Flat Number : {props.flatName}</h6>
                </div>
                <div className="card-body">
                    <div className="green-text darken-1 card-text"><span className="light-blue-text darken-2">Status :</span> On Rent</div>
                    <div className="light-blue-text card-text">Rent Per Month : <span className="pink-text">{renterDetail.rentPerMonth} tk</span></div>
                </div>
            </div>
        )
    }
    else {
        return (
            <div className="card mb-2">
                <div className={"card-header text-white " + classes.renterHeaderInActive} style={{ cursor: 'pointer' }}>
                <h4 className="card-title">No Renter</h4>
                    <h6 className="card-subtitle">Flat Number : {props.flatName}</h6>
                </div>
                <div className="card-body">
                    <div className="green-text darken-1 card-text"><span className="light-blue-text darken-2">Status :</span>Not on Rent</div>
                    <div className="light-blue-text card-text">Rent Per Month : <span className="pink-text">N/A</span></div>
                </div>
            </div>
        )
    }

}
